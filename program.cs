using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;
using System.IO;

namespace demo_test
{
    class Program
    {
        static void readfile(byte[] arr)
        {
            StreamWriter w = new StreamWriter("test");

        }
        static void Main(string[] args)
        {
            SerialPort p = new SerialPort();
            p.PortName = "COM3";
            p.Parity = Parity.None;
            p.StopBits = StopBits.One;
            p.BaudRate = 9600;

            try
            {
                p.Open();
            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            char[] test = { 't', 'e', 's', 't', '\n', '\r' };
            p.Write(test, 0, test.Length);
            Thread.Sleep(200);
            int ch;
            int[] twolast = new int[2];
            int i = 0;
            Console.WriteLine("press enter.");
            Console.ReadLine();
            while (twolast[0] != '\\' && twolast[1] != '>')
            {
                ch = p.ReadChar();
                if (ch != 0x0D) Console.Write((char)ch);

                if (i == twolast.Length) i = 0;

                twolast[i] = ch;
                ++i;
            }

            Console.WriteLine("----------------------------------");
            Console.ReadLine();

            p.Close();
        }
    }
}